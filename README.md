![Screenshot](screenshot.png)

# COVID Computer Lab Tracking

This was created to help staff efficiently keep track of the computer lab at my workplace. It has an interactive UI that tracks open PCs, counts down patron time, and ensures PCs are disinfected before the next patron.

Technologies used: HTML / CSS / JavaScript
