// Function used to set up events and timers
function setup() {
  // Grab all PCs in the table
  var pcs = document.getElementsByClassName("pc");

  // Make each PC clickable
  for (var i = 0; i < pcs.length; i++) {
    pcs[i].addEventListener("click", tblClick);
  }

  // Add timer to count down the amount of time left on PC
  setInterval(countdownTimers, 60 * 1000);

  // Add timer to show flashing timer
  setInterval(flashTimers, 1 * 1000);
}

// Function used to handle when PC is clicked on table
function tblClick(event) {
  // Get the sender object
  var pc = event.target;

  // Add 2 hour countdown on clicked open PC
  if (pc.classList.contains("open")) {
    pc.className = "pc active";
    pc.innerHTML += "<br /><span class='timer'>2:00</span>";

    // Close PC for disinfecting on clicked active PC
  } else if (pc.classList.contains("active")) {
    pc.className = "pc closed";
    pc.innerHTML = pc.id;

    // Re-open PC after it has been disinfected
  } else if (pc.classList.contains("closed")) {
    pc.className = "pc open";
  }
}

// Function used to count down the timers
function countdownTimers() {
  // Get timers
  var timers = document.getElementsByClassName("timer");

  // Count down a minute from each timer
  for (var i = 0; i < timers.length; i++) {
    // Grab parts of timer
    var hours = timers[i].innerHTML.split(/\D/)[0];
    var separator = timers[i].innerHTML.replace(/[^\D]/g, "");
    var minutes = timers[i].innerHTML.split(/\D/)[1];

    // Subtract a minute from timer down to 0:00
    minutes -= 1;
    if (minutes < 0) {
      hours -= 1;
      minutes = 60;
      if (hours < 0) {
        hours = 0;
        minutes = 0;
      }
    }

    // Display new timer
    timers[i].innerHTML = hours + separator + String(minutes).padStart(2, '0');
  }
}

// Function used to flash the colon within the timers
function flashTimers() {
  // Grab timers
  var timers = document.getElementsByClassName("timer");

  // Set up target/replace for ':' and ' ' and flip if necessary
  var target = ":"
  var replace = " ";

  if (timers.length > 0 && timers[0].innerHTML.includes(" ")) {
    target = " ";
    replace = ":";
  }

  // Change all timers to show flashing
  for (var i = 0; i < timers.length; i++) {
    timers[i].innerHTML = timers[i].innerHTML.replace(target, replace);
  }
}
